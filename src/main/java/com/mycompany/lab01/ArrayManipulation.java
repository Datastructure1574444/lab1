/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class ArrayManipulation {

    static int[] numbers = {5,8,3,2,7};
    static String[] names = {"Alice","Bob","Charlie","David"};
    static double[] values = new double[4];

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //EnhancedForloop for print
        for (int i : numbers){
            System.out.print(i+" ");
        }
        System.out.println();
        
        //NormalForloop
        int sum=0;
        for(int i=0; i<numbers.length;i++){
            System.out.print(numbers[i]);
            sum = sum + numbers[i];
            if(i== numbers.length-1){
                System.out.println("Sum in numbers Array is = "+sum);
            }
        }

        for(int i=0; i<names.length;i++){
            System.out.println(names[i]);
        }
        System.out.println();
        values[0] = 1.23;
        values[1] = 1.33;
        values[2] = 4.3;
        values[3] = 3.5;

        double maximum = 0;
        for(int i=0; i<values.length;i++){
            if(values[i]>maximum){
                maximum = values[i];
            }
        }

        System.out.println("The Maximum Values is = "+maximum);

        System.out.println();
        String[] reversedNames = new String[names.length];

        for(int i=0;i<names.length;i++){
            reversedNames[i] = names[names.length-1-i];
        }

        System.out.println("The reverse Element of reversedName Array ");
        for(int i=0; i<reversedNames.length;i++){
            System.out.println(reversedNames[i]);
        }   
        System.out.println();
        for(int i=0;i<numbers.length-1;i++){
            for(int j=0;j<numbers.length-1-i;j++){
                int rightNumber;
                if(numbers[j+1]<numbers[j]){
                    rightNumber = numbers[j];
                    numbers[j] = numbers[j+1];
                    numbers[j+1] = rightNumber;
                    
                }
            }
        }
        
        System.out.println("The sorted ascending order Element of numbers Array ");
        for(int i=0;i<numbers.length;i++){
            System.out.println(numbers[i]);
        }

    }

}
