/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab01;

/**
 *
 * @author HP
 */
public class Lab1_2 {
    public static void main(String[] args) {
        int[] arr = {1,0,2,3,0,4,5,0};

        for(int i=0;i<arr.length-1;i++){
            if(arr[i]==0){
                for(int j=1;j<arr.length-i;j++){ //execute loop involve the range of i
                    arr[arr.length-j] = arr[arr.length-j-1]; // the last element will be the element before itself 
                }
                arr[i+1]=0;
                i++; // if found 0 do i+1 becuase i+1 was 0 and across to anothor i
            }
        }

        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}
